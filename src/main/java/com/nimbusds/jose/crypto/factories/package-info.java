/**
 * JWS verifier and JWE decrypter factories for use by the JOSE / JWT processor
 * framewor.
 */
package com.nimbusds.jose.crypto.factories;